import { Phone } from "./phone";
import { Address } from "./address";
import { Mail } from "./mail";

export class Person {
    private name: string;
    private lastName: string;
    private age: number;
    private dni: string;
    private birthday: string;
    private favColor: string;
    private gender: string;
    private addresses: Address;
    private mails: Mail;
    private phones: Phone;
    private notes: string;

    constructor(
        _name: string,
        _lastName: string,
        _age: number,
        _dni: string,
        _birthday: string,
        _favColor: string,
        _gender: string,
        _addresses: Address,
        _mails: Mail,
        _phones: Phone,
        _notes: string
    ) {

        this.name = _name
        this.lastName = _lastName
        this.age = _age
        this.dni = _dni
        this.birthday = _birthday
        this.favColor = _favColor
        this.gender = _gender
        this.addresses = _addresses
        this.mails = _mails
        this.phones = _phones
        this.notes = _notes
    }

    //Getters y Setters

    //Name
    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    //Last Name
    public getLastName(): string {
        return this.lastName;
    }

    public setLastName(lastName: string): void {
        this.lastName = lastName;
    }

    //Age
    public getAge(): number {
        return this.age;
    }

    public setAge(age: number): void {
        this.age = age;
    }

    //DNI
    public getDni(): string {
        return this.dni;
    }

    public setDni(dni: string): void {
        this.dni = dni;
    }

    //Birthday
    public getBirthday(): string {
        return this.birthday;
    }

    public setBirthday(birthday: string): void {
        this.birthday = birthday;
    }

    //Favourite color
    public getFavColor(): string {
        return this.favColor;
    }

    public setFavColor(favColor: string): void {
        this.favColor = favColor;
    }

    //Gender
    public getGender(): string {
        return this.gender;
    }

    public setGender(gender: string): void {
        this.gender = gender;
    }

    //Address
    public getAddresses(): Address {
        return this.addresses;
    }

    public setAddresses(addresses: Address): void {
        this.addresses = addresses;
    }

    //Mail
    public getMails(): Mail {
        return this.mails;
    }

    public setMails(mails: Mail): void {
        this.mails = mails;
    }

    //Phone
    public getPhone(): Phone {
        return this.phones;
    }

    public setPhone(phones: Phone): void {
        this.phones = phones;
    }

    //Notes
    public getNotes(): string {
        return this.notes;
    }

    public setNotes(notes: string): void {
        this.notes = notes;
    }
}