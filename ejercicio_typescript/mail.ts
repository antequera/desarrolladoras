export class Mail {
    private mailType: string;
    private mailAddress: string;

    constructor(_mailType: string, _mailAddress: string) {
        this.mailType = _mailType
        this.mailAddress = _mailAddress
    }

    //Getters y Setters
    public getMailType(): string {
        return this.mailType;
    }

    public setMailType(mailType: string): void {
        this.mailType = mailType;
    }

    public getMailAddress(): string {
        return this.mailAddress;
    }

    public setMailAddress(mailAddress: string): void {
        this.mailAddress = mailAddress;
    }
}