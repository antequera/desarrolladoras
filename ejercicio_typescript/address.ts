export class Address {
    private street: string;
    private flatNumber: number;
    private floor: number;
    private letter: string;
    private zipCode: number;
    private town: string;
    private city: string

    constructor(
        _street: string,
        _flatNumber: number,
        _floor: number,
        _letter: string,
        _zipCode: number,
        _town: string,
        _city: string
    ) {

        this.street = _street
        this.flatNumber = _flatNumber
        this.floor = _floor
        this.letter = _letter
        this.zipCode = _zipCode
        this.town = _town
        this.city = _city
    }

    //Getters y Setters
    public getStreet(): string {
        return this.street;
    }

    public setStreet(street: string): void {
        this.street = street;
    }

    public getFlatNumber(): number {
        return this.flatNumber;
    }

    public setFlatNumber(flatNumber: number): void {
        this.flatNumber = flatNumber;
    }

    public getFloor(): number {
        return this.floor;
    }

    public setFloor(floor: number): void {
        this.floor = floor;
    }

    public getLetter(): string {
        return this.letter;
    }

    public setLetter(letter: string): void {
        this.letter = letter;
    }

    public getZipCode(): number {
        return this.zipCode;
    }

    public setZipCode(zipCode: number): void {
        this.zipCode = zipCode;
    }

    public getTown(): string {
        return this.town;
    }

    public setTown(town: string): void {
        this.town = town;
    }

    public getCity(): string {
        return this.city;
    }

    public setCity(city: string): void {
        this.city = city;
    }
} 