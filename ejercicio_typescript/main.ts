import { Person } from './person';
import { Phone } from './phone';
import { Address } from './address';
import { Mail } from './mail';

//Phones
let phoneMarisa = new Phone("xiaomi", 665555555);
let phoneRuben = new Phone("iPhone", 600123789);
let phoneBryan = new Phone("Samsung", 611589720);
let newPhone = new Phone("xiaomi", 666666666);

//Addresses
let adressMarisa = new Address("Malaga City", 17, 3, "A", 29004, "Malaga", "Malaga");
let adressRuben = new Address("Malaga City", 17, 3, "A", 29004, "Malaga", "Malaga");
let adressBryan = new Address("Madrid City", 24, 2, "C", 28020, "Torrelodones", "Madrid");
let newAddress = new Address("Murcia City", 33, 8, "B", 30009, "Murcia", "Murcia");

//Mails
let mailMarisa = new Mail("gmail", "marisa@gmail.com");
let mailRuben = new Mail("gmail", "ruben@gmail.com");
let mailBryan = new Mail("gmail", "bryan@gmail.com");
let newMail = new Mail("gmail", "ruben_bye@gmail.com");

//Persons
let Marisa = new Person(
    "Marisa",
    "Antequera",
    29,
    "12345678A",
    "22 March",
    "pink",
    "female",
    adressMarisa,
    mailMarisa,
    phoneMarisa,
    "Marisa is the girlfriend of Ruben"
);

let Ruben = new Person(
    "Ruben",
    "Holland",
    26,
    "87654321Z",
    "24 December",
    "red",
    "male",
    adressRuben,
    mailRuben,
    phoneRuben,
    "Ruben is the boyfriend of Marisa"
);

let Bryan = new Person(
    "Bryan",
    "Antequera",
    19,
    "77777777H",
    "12 April",
    "black",
    "male",
    adressBryan,
    mailBryan,
    phoneBryan,
    "Bryan is the brother of Marisa"
);

//Logs
console.log(Marisa)
console.log(Ruben)
console.log(Bryan)

//Find Person by DNI
console.log("Change data from DNI: 87654321Z")

let personList: Array<Person> = new Array(Marisa, Ruben, Bryan);

let selectedPerson: Person;
for (let person of personList) {
    if (person.getDni() == "87654321Z") {
        selectedPerson = person;
        selectedPerson.setPhone(newPhone);
        selectedPerson.setAddresses(newAddress);
        selectedPerson.setMails(newMail);
        console.log(selectedPerson);
    }
}