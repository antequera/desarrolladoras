export class Phone {
    private phoneType: string;
    private phoneNumber: number;

    constructor(_phoneType: string, _phoneNumber: number) {
        this.phoneType = _phoneType
        this.phoneNumber = _phoneNumber
    }

    //Getters y Setters
    public getPhoneType(): string {
        return this.phoneType;
    }

    public setPhoneType(phoneType: string): void {
        this.phoneType = phoneType;
    }

    public getPhoneNumber(): number {
        return this.phoneNumber;
    }

    public setPhoneNumber(phoneNumber: number): void {
        this.phoneNumber = phoneNumber;
    }
}