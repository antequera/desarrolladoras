import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';

@Component({
  selector: 'app-person-agenda',
  templateUrl: './person-agenda.component.html',
  styleUrls: ['./person-agenda.component.css']
})

export class PersonAgendaComponent implements OnInit {
  personForm : FormGroup;
  contactList: Array<any> = [];
  contactPosition: any;
  contactOption: string = 'add';
  constructor(
    private dateAdapter: DateAdapter<Date>,
    private fb: FormBuilder
    ) {

      this.dateAdapter.setLocale('en-GB'); //dd/mm/yyyy


      this.personForm = this.fb.group({
        name: ['', [Validators.required, Validators.minLength(3)]],
        lastName: ['', [Validators.required, Validators.minLength(3)]],
        age: ['', [Validators.required, Validators.minLength(3), Validators.min(0), Validators.max(125)]],
        dni: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]],
        birthday: ['', Validators.required],
        favColor: ['', [Validators.required, Validators.minLength(3)]],
        gender: ['', Validators.required]
    });

  }

  get contact() {
    return this.personForm.controls;
  }

  addContact() {
    if (this.contactOption === 'add') {
      this.contactList.push(this.personForm.value);
    } else {
      this.contactList[this.contactPosition] = this.personForm.value;
      this.contactOption = 'add';
    }

    this.personForm.reset();
  }

  removeContact(arrayLocation: number) {
    this.contactList.splice(arrayLocation, 1);
  }

  editContact(arrayLocation: number) {
    this.personForm.patchValue(this.contactList[arrayLocation]);
    this.contactOption = 'edit';
    this.contactPosition = arrayLocation;
  }

  ngOnInit() {}

}
