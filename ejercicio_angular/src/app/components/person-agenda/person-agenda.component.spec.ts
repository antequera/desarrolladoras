import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonAgendaComponent } from './person-agenda.component';

describe('PersonAgendaComponent', () => {
  let component: PersonAgendaComponent;
  let fixture: ComponentFixture<PersonAgendaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonAgendaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonAgendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
