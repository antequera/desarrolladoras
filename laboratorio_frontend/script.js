// Mostrar valoración
function valoracion() {
    var valor = document.getElementById("valor").value;
    var valoracionRango = "Has valorado con: " + valor + " puntos";
    alert(valoracionRango);
}

// Mostrar cuenta bancaria
function banco() {
    var pais = document.getElementById("pais").value;
    var iban = document.getElementById("ciban").value;
    var entidad = document.getElementById("enti").value;
    var sucursal = document.getElementById("suc").value;
    var controlDigit = document.getElementById("dc").value;
    var numeroCuenta = document.getElementById("cuenta").value;

    var cuentaBancaria = "Le informamos que su cuenta bancaria es: "
        + pais
        + iban
        + "-"
        + entidad
        + "-"
        + sucursal
        + "-"
        + controlDigit
        + "-"
        + numeroCuenta;
    alert(cuentaBancaria);
}


// Mostrar día de la semana
function diaSemana() {
    var fecha = document.getElementById("dt").value;
    const d = new Date(fecha);
    let day = d.getDay();

    switch (day) {
        case 1:
            alert("La fecha seleccionada en el elemento de fecha es un Lunes");
            break;
        case 2:
            alert("La fecha seleccionada en el elemento de fecha es un Martes");
            break;
        case 3:
            alert("La fecha seleccionada en el elemento de fecha es un Miércoles");
            break;
        case 4:
            alert("La fecha seleccionada en el elemento de fecha es un Jueves");
            break;
        case 5:
            alert("La fecha seleccionada en el elemento de fecha es un Viernes");
            break;
        case 6:
            alert("La fecha seleccionada en el elemento de fecha es un Sábado");
            break;
        case 7:
            alert("La fecha seleccionada en el elemento de fecha es un Domingo");
            break;
    }
}

// Código Postal
function codigoPostal() {
    var Cpostal = document.getElementById("cp").value;
    var provincia = document.getElementById("localidad").value;

    let formulario = document.getElementById("telefonica");

    var numeros = /^[0-9]+$/;

    var CpProvincias = {
        01: "Alava", 02: "Albacete", 03: "Alicante", 04: "Almeria", 05: "Avila",
        06: "Badajoz", 07: "Baleares", 08: "Barcelona", 09: "Burgos", 10: "Caceres",
        11: "Cadiz", 12: "Castellon", 13: "Ciudad Real", 14: "Cordoba", 15: "Coruña",
        16: "Cuenca", 17: "Gerona", 18: "Granada", 19: "Guadalajara", 20: "Guipuzcoa",
        21: "Huelva", 22: "Huesca", 23: "Jaen", 24: "Leon", 25: "Lerida",
        26: "La Rioja", 27: "Lugo", 28: "Madrid", 29: "Malaga", 30: "Murcia",
        31: "Navarra", 32: "Orense", 33: "Asturias", 34: "Palencia", 35: "Las Palmas",
        36: "Pontevedra", 37: "Salamanca", 38: "Santa Cruz de Tenerife", 39: "Cantabria", 40: "Segovia",
        41: "Sevilla", 42: "Soria", 43: "Tarragona", 44: "Teruel", 45: "Toledo",
        46: "Valencia", 47: "Valladolid", 48: "Vizcaya", 49: "Zamora", 50: "Zaragoza",
        51: "Ceuta", 52: "Melilla"
    };

    var validacionProvincia = CpProvincias[parseInt(Cpostal.substring(0, 2))];  // provincia

    // Validación Código Postal
    if (Cpostal == "" || Cpostal == " ") {
        alert("El Código Postal no puede quedar vacío o con espacios");
        formulario.reset();
    }

    else if (!Cpostal.match(numeros)) {
        alert("El Código Postal debe ser un número");
        formulario.reset();
    }

    else if (Cpostal.length != 5) {
        alert("El Código Postal debe tener 5 dígitos");
        formulario.reset();
    }

    else if (Cpostal >= 52999 || Cpostal <= 01000) {
        alert("Introduce un número correcto en el campo Codigo Postal");
        formulario.reset();
    }



    // Validación Localidad
    if (provincia == "" || provincia == " ") {
        alert("El campo Localidad no puede quedar vacío o con espacios");
        formulario.reset();
    }

    else if (provincia.match(numeros)) {
        alert("La Localidad no puede ser un número");
        formulario.reset();
    }

    else if (!Object.values(CpProvincias).includes(provincia)) {
        alert("Introduce una Provincia válida");
        formulario.reset();
    }

    /*else if (validacionProvincia != provincia) {
        alert("La localidad no se corresponde con el Código Postal introducido");
        formulario.reset();
    }*/


    // Validar Código Postal - Provincia
    if (validacionProvincia == provincia) {
        let verde = document.createElement("h4");
        verde.textContent = provincia + '\n' + ' ES CORRECTA';
        verde.setAttribute("id", "verde");
        document.getElementById("cpp").appendChild(verde);
        document.getElementById("verde").style.color = "#00e600";

    } else {
        let rojo = document.createElement("h4");
        rojo.textContent = provincia + '\n' + " ES ERRÓNEA";
        rojo.setAttribute("id", "rojo");
        document.getElementById("cpp").appendChild(rojo);
        document.getElementById("rojo").style.color = "#ff0000";
    }


}

function resultadoReset() {
    document.getElementById("cpp").remove();
}
