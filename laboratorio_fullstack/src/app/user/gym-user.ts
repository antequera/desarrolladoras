export class GymUser {
  name: string;
  lastName: string;
  userID: number;
  dni: string;
  phone: number;
  gender: string;

  constructor(
      _name: string,
      _lastName: string,
      _userID: number,
      _dni: string,
      _phone: number,
      _gender: string,
      ) {

          this.name = _name
          this.lastName = _lastName
          this.userID = _userID
          this.dni = _dni
          this.phone = _phone
          this.gender = _gender
        }

  //Getters y Setters

  //Name
  public getName(): string {
    return this.name;
    }

  public setName(name: string): void {
    this.name = name;
    }

  //Last Name
  public getLastName(): string {
      return this.lastName;
    }

  public setLastName(lastName: string): void {
      this.lastName = lastName;
    }

  //User ID
  public getUserID(): number {
      return this.userID;
    }

  public setUserID(userID: number): void {
      this.userID = userID;
    }

  //DNI
  public getDni(): string {
      return this.dni;
    }

  public setDni(dni: string): void {
      this.dni = dni;
    }

  //Phone number
  public getPhone(): number {
      return this.phone;
    }

  public setPhone(phone: number): void {
      this.phone = phone;
    }

  //Gender
  public getGender(): string {
      return this.gender;
    }

  public setGender(gender: string): void {
      this.gender = gender;
    }
}
