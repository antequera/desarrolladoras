import { Component, OnInit } from '@angular/core';
import { GymUser } from 'src/app/user/gym-user';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-gym-form',
  templateUrl: './gym-form.component.html',
  styleUrls: ['./gym-form.component.css']
})

export class GymFormComponent implements OnInit {
  user: FormGroup;
  userList: Array<GymUser> = [];
  userPosition: any;
  userOption: string = 'add';

  constructor(
    private fb: FormBuilder
    ) {
      this.user = this.fb.group({
        name: ['', [Validators.required, Validators.minLength(3)]],
        lastName: ['', [Validators.required, Validators.minLength(3)]],
        userID: ['', Validators.required],
        dni: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]],
        phone: ['', Validators.required],
        gender: ['', Validators.required]
      });
}

get contact() {
  return this.user.controls;
}

idTest(): boolean{
  let variable = this.user.controls["userID"].value;
  for (let person of this.userList) {
    if (variable == person.userID) {
        alert("User ID repeated: " + variable);
        return true
    }
  }
  return false
}

addUser() {
  if (!this.idTest()) {
    if (this.userOption === 'add') {
      this.userList.push(this.user.value);
    } else {
      this.userList[this.userPosition] = this.user.value;
      this.userOption = 'add';
    }

    this.user.reset();
  }
}

removeUser(arrayLocation: number) {
  this.userList.splice(arrayLocation, 1);
}

editUser(arrayLocation: number) {
  this.user.patchValue(this.userList[arrayLocation]);
  this.userOption = 'edit';
  this.userPosition = arrayLocation;
}

ngOnInit() {}

}
